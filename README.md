# StickerBase







Liste von Stickern

| Name | Text | Ansicht | Abmessungen | Lizenz |
| ------ | ------ | ------ | ------ | ------ |
| [rauch_unbedenklich.eps](https://gitlab.com/NetNudel/stickerbase/blob/master/sticker/rauch_unbedenklich.eps) | Mögliches Austreten von Rauch aus dem Gerät ist normal und unbedenklich. | [rauch_unbedenklich.jpg](https://gitlab.com/NetNudel/stickerbase/raw/master/sticker/rauch_unbedenklich.jpg) | 100x35mm | N/A |
| [cyber.eps](https://gitlab.com/NetNudel/stickerbase/blob/master/sticker/cyber.eps) | CYBER | [cyber.jpg](https://gitlab.com/NetNudel/stickerbase/raw/master/sticker/cyber.jpg) | 100x30mm | CC-BY-NC fnordeingang e.V. |
|  |  |  |  |  |